# Risorse non condivisibili

Proponiamo due consegne di cui la prima (esercitzione di laboratorio del 31/10/2018) è requisito della seconda.

## Due task in time sharing

Si consegni nella cartella `Tasks` (può essere un package `Java` se si preferisce) quanto segue. Il sorgente `TwoTask.java` in cui a due Task (una qualche classe che estenda `Runnable`) eseguiti poi come `Thread` viene concesso in modo **mutuamente esclusivo** il tempo macchina (quando uno è in esecuzione l'altro sta fermo e viceversa); ad ogni task vien4e concesso un tempo random che va da 1 a 4 secondi; terminato tale tempo la CPU viene concessa all'altro task; ai costruttori viene passato un int in modo che si possa predisporre la durata del task; quando un task entra in esecuzione (anche dopo aver finito un'attesa) stampa a video un messaggio segnalando la sua esecuzione. Va allegato anche il solito file `README.md`.

## Risorsa non condivisibile

Partendo dalla precedente esercitazione nella cartella `Risorsa` si consegna quanto segue. Un file `Main.java` in cui la classe eseguibile serva ai nostri test (più test più classi: `Main1`, `Main2`, ...); un file `Task.java` in cui viene a definirsi la classe per i `Task` (vedasi precedente esercitazione); un `Resource.java` per una generica risorsa non condivisibile (da realizzarsi coi semafori), avremo due oggetti di tale classe. Una simulazione (classe `Main`) prevede due task in esecuzione in modo che ad essi il tempo macchina venga prima dato all'uno e poi all'altro (non possono essere entrambi in esecuzione); il primo task dopo un tempo random tenta di acquisire la prima delle due risorse per poi acquisire la seconda risorsa; il secondo task dopo un tempo random tenta di acquisire la seconda risorsa per poi acquisire la prima risorsa; entrambi i task quando acqusiscono una risorsa la tengono un tempo ragionevole (esperibile da chi quarda il terminale); un task puà avere acquisito in un determinato istante entrambe le risorse. Sono interesanti le simulazione che portano ad un blocco del sistema. Per le figure si faccia riferimento agli appunti: [Programmazione Concorrente](https://drive.google.com/open?id=1Pr9mUD6CM6Kgn4WjJAUedTJb-qa29_5N).

## Valutazione

Oltre a richiedere la pulizia del codice, la documentazione e guardando alle scelte operate, i progetti
devono essere consegnati nelle loro cartelle con i file `README.md` in cui si dà breve descrizione del
progetti con attenzione alle scelte operate. Le valutazioni eccellenti (8, 9, 10) deriveranno da un consistente apporto personale e creativo.
